package cat.epiaedu.damviod.pmdm;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.Timer;

import java.util.Iterator;

public class JocTest extends ApplicationAdapter implements ApplicationListener{

	SpriteBatch batch;
	Texture img;
	Sprite sprite;
	TextureAtlas texAtlas;
	private OrthographicCamera camera;
	float stateTime;
	float currentFrame;
	String currentAtlasKey;
	private float elapsedTime = 0;
	private Rectangle player;
	Animation animation;
	public Animation<TextureRegion> runningAnimation;
	private Sound motor;
	private Sound accel;
	boolean on;
	private BitmapFont font;
	static float accelerate = 1;
	static final float slow = -10;
	static final float speed = 0.2f;
	float cont;
	boolean go = false;
	long soundID;
	GestureDetector gd;

	//runningAnimation = new Animation<TextureRegion>(0.033f,atlas.findRegions("animation_sheet.png"),Animation.PlayMode.Looping);
	@Override
	public void create () {
		batch = new SpriteBatch();

		font = new BitmapFont();
		img = new Texture("badlogic.jpg");
		motor = Gdx.audio.newSound(Gdx.files.internal("engine-idle.wav"));
		accel = Gdx.audio.newSound(Gdx.files.internal("engine-running.wav"));
		texAtlas = new TextureAtlas(Gdx.files.internal("pack.atlas"));
		TextureAtlas.AtlasRegion region = texAtlas.findRegion("Hit1");
		animation = new Animation(1/15f,texAtlas.getRegions());
		sprite = new Sprite(region);
		sprite.setPosition(120,100);
		sprite.scale(0.5f);



		camera = new OrthographicCamera();
		camera.setToOrtho(false, 800, 480);

		player = new Rectangle();
		player.x = 800 / 2 - 128 / 2;
		player.y = 128;
		player.width = 128;
		player.height = 128;

	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		font.setColor(Color.WHITE);
		font.draw(batch, cont + "km/h", 25, 160);
		elapsedTime += Gdx.graphics.getDeltaTime();
		batch.draw((TextureRegion) animation.getKeyFrame(elapsedTime, true), player.x, player.y);
		batch.end();

		if(Gdx.input.isTouched()) {
			Vector3 touchPos = new Vector3();
			touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
			camera.unproject(touchPos);
			player.x = touchPos.x - 256/2;
			player.y = touchPos.y - 128/2;

		}
		if(Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT))
		{
			startEngine();
			if(on == false) // parar el motor
			{
				stopEngine();
			}
		}
		if(Gdx.input.isKeyPressed(Input.Keys.SPACE))
		{
			if(on == true && cont <200)
			{
				accelerate();
				cont ++;
			}
		}
		if(Gdx.input.isKeyPressed(Input.Keys.CONTROL_RIGHT))
		{
			if(cont >0)
			{
				desacelerate();
				cont --;
			}


		}

		if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) player.x -= 200 * Gdx.graphics.getDeltaTime();
		if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) player.x += 200 * Gdx.graphics.getDeltaTime();

		if(player.x < 0) player.x = 0;
		if(player.y <0 ) player.y = 0;
		if(player.y >380) player.y = 380;
		if(player.x > 640 - 128) player.x = 640 - 128;


	}
	public void accelerate()
	{
		if (!go)
		{
			soundID = accel.play();
			go = true;
		}
		float accelerate2;
		accelerate2 = accelerate += speed * Gdx.graphics.getDeltaTime();
		accel.setPitch(soundID,accelerate2);

		//accel.play();
	}
	public void desacelerate()
	{
		if(go)
		{
			soundID = accel.play();
			go = false;
		}

		float accelerate2;
		accelerate2 = accelerate -= speed * Gdx.graphics.getDeltaTime();
		accel.setPitch(soundID,accelerate2);

		//accel.play();
	}
	public void stopEngine()
	{
		motor.stop();
	}
	public void startEngine()
	{
		motor.play();
		on = true;
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		img.dispose();
		texAtlas.dispose();
		motor.dispose();
		accel.dispose();
	}
}
